# ncdrive

Ruby script for interacting with NextCloud API

ncdrive wants to run alongside a dot file:

- $HOME/.ncdrive.yml or .ncdrive.yml in same directory

   YAML file containing API URL & credentials

This script relies on the nextcloud gem

     Usage: ncdrive.rb [OPTIONS] <FILE(S)>
         -d, --directory=DIRECTORY        Set base path
         -h, --help                       Show this help message
         -v, --version                    Print verson

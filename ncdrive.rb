#!/usr/bin/env ruby

# NextCloud API Client
# Daniel Bowling <swaggboi@slackware.uk>
# Jul 2020

require 'optparse'
require 'yaml'
require 'nextcloud'

# Variables
cmd       = File.basename($PROGRAM_NAME)
conf      = {}
directory = ''
options   = {}
version   = '0.1'

# Parse options
OptionParser.new do |opts|
  opts.banner = "Usage: #{cmd} [OPTIONS] <FILE(S)>"

  opts.on('-d', '--directory=DIRECTORY', 'Set base path') do |dir|
    options[:directory] = dir
  end

  opts.on('-h', '--help', 'Show this help message') do
    puts opts
    exit
  end

  opts.on('-v', '--version', 'Print verson') { options[:version] = true }
end.parse!

# Print version if -v specified
if options[:version]
  puts version
  exit
end

# Check for arguments
unless ARGV[0]
  puts "#{cmd}: No arguments given, try -h or --help"
  exit(64)
end

# Handle arguments
#action = ARGV.shift.downcase
files  = ARGV

# Load config file
%w[yaml yml].each do |ext|
  conf =
    if File.file?("#{ENV['HOME']}/.ncdrive.#{ext}")
      YAML.load_file("#{ENV['HOME']}/.ncdrive.#{ext}")
    elsif File.file?(".ncdrive.#{ext}")
      YAML.load_file(".ncdrive.#{ext}")
    end
end

# Or not...
if conf.nil?
  puts "#{cmd}: YAML dotfile not found"
  exit(65)
end

# API Creds
webdav = Nextcloud.webdav(
  url:      conf['url'],
  username: conf['username'],
  password: conf['password']
)

# Handle the directory
directory = options[:directory] || '/'
# Negative match leading forward slash
directory = "/#{directory}" unless directory.match(%r{^/})
# Negative match trailing forward slash
directory = "#{directory}/" unless directory.match(%r{/$})

# TODO: Break out upload action into it's own function
# Main loop
files.each do |file|
  # Set the filename string from first file
  filename = directory + File.basename(file)
  # Open the file, read-only
  fileguts = File.open(file, 'r')

  # Send it
#  if action.eql?('upload')
    print "#{filename}:\t"
    puts webdav.directory.upload(filename, fileguts.read)[:status]
#  end

  # Close file
  fileguts.close
end
